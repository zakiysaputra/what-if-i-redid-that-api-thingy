API CALL

1. Signup/Login

api/auth/signup
api/auth/signin

2. Get job list API

api/get/positions/{id}

3. Get job detail API

api/get/positions

4. Download job list API

api/get/positions/download

Initially, 2-3-4 was supposed to require authentication by 1, but attempts so far to do that has failed. 4 was done partially successful due to
issues in data given (especially the HTML tag and etc in description)