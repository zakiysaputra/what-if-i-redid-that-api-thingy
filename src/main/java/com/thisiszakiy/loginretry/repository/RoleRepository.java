package com.thisiszakiy.loginretry.repository;

import com.thisiszakiy.loginretry.model.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(String name);
}