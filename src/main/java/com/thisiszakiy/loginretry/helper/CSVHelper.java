package com.thisiszakiy.loginretry.helper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;

import com.thisiszakiy.loginretry.model.Job;

public class CSVHelper {

  public static ByteArrayInputStream jobsToCSV(List<Job> jobs) {
    final CSVFormat format = CSVFormat.DEFAULT.withQuoteMode(QuoteMode.MINIMAL);

    try (ByteArrayOutputStream out = new ByteArrayOutputStream();
        CSVPrinter csvPrinter = new CSVPrinter(new PrintWriter(out), format);) {
      for (Job job : jobs) {
        List<String> data = Arrays.asList(
              job.getId(),
              job.getType(),
              job.getUrl(),
              job.getCreated_at(),
              job.getCompany(),
              job.getCompany_url(),
              job.getLocation(),
              job.getTitle(),
              job.getDescription(),
              job.getHow_to_apply(),
              job.getCompany_logo()
            );

        csvPrinter.printRecord(data);
      }

      csvPrinter.flush();
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new RuntimeException("fail to import data to CSV file: " + e.getMessage());
    }
  }
}