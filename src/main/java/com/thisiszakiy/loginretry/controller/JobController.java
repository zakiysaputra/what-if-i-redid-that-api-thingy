package com.thisiszakiy.loginretry.controller;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.csv.CsvSchema.Builder;
import com.thisiszakiy.loginretry.model.Job;
import com.thisiszakiy.loginretry.helper.CSVHelper;

@Controller
@RequestMapping("/api/get")
public class JobController {
	 
	 // https://medium.com/@nutanbhogendrasharma/consume-rest-api-in-spring-boot-web-application-354c404850f0
	 @RequestMapping("/positions/{id}")
	 @ResponseBody
	 private Map<String, String> getJobDetails(@PathVariable String id) {
		 String uri = "http://dev3.dansmultipro.co.id/api/recruitment/positions/" + id;
		 RestTemplate restTemplate = new RestTemplate();
		  
		 Job job = restTemplate.getForObject(uri, Job.class);
		 
		 HashMap<String, String> map = new HashMap<>();
		  
		 map.put("jobid: ", job.getId());
		 map.put("type: ", job.getType());
		 map.put("url: ", job.getUrl());
		 map.put("strDateTime: ", job.getCreated_at());
		 map.put("company: ", job.getCompany());
		 map.put("companyURL: ", job.getCompany_url());
		 map.put("location: ", job.getLocation());
		 map.put("title: ", job.getTitle());
		 map.put("description: ", job.getDescription());
		 map.put("how to apply: ", job.getHow_to_apply());
		 map.put("company logo: ", job.getCompany_logo());
		 return map;
	 }
	 
	 @RequestMapping("/positions")
	 @ResponseBody
	 private String getJobDetails() {
		 String uri = "http://dev3.dansmultipro.co.id/api/recruitment/positions.json";
		 RestTemplate restTemplate = new RestTemplate();
		 
		 final String response = restTemplate.getForObject(uri, String.class);
		 
		 return response;
	 }
	 
	 //https://www.bezkoder.com/spring-boot-download-csv-file/
	 @RequestMapping(path= "/positions/download", method= RequestMethod.GET)
	 @ResponseBody
	 private ResponseEntity<Resource> downloadJobDetails() throws JsonMappingException, JsonProcessingException {
		 String uri = "http://dev3.dansmultipro.co.id/api/recruitment/positions.json";
		 RestTemplate restTemplate = new RestTemplate();
		 
		 
		 final String response = restTemplate.getForObject(uri, String.class);
		 
		 ObjectMapper mapper = new ObjectMapper();
		 List<Job> participantJsonList = mapper.readValue(response, new TypeReference<List<Job>>(){});
		 
		 String download = "jobsDownload.csv";
		 CSVHelper helper = new CSVHelper();
		 InputStreamResource file = new InputStreamResource(helper.jobsToCSV(participantJsonList));
		 
		 return ResponseEntity.ok()
			        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + download)
			        .contentType(MediaType.parseMediaType("application/csv"))
			        .body(file);
	 }
}